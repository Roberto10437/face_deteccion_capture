import cv2
import os
import sys
from time import sleep
import time

cascPath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascPath)
video_capture = cv2.VideoCapture('rtsp://admin:colocolo@192.168.0.100/live')


# For each person, enter one numeric face id
name = input("¿Caual es el nombre? ")
print("\n [INFO] Inicializando por favor mire a la camara ...")
dirName = "./images/" + name
print(dirName)
if not os.path.exists(dirName):
    os.makedirs(dirName)
    print("Directory Created")
else:
    print("Name already exists")
    sys.exit()

count= 1
while True:
    if not video_capture.isOpened():
        print('camara no disponible.')
        sleep(5)
        pass

    # Capture frame-by-frame
    ret, frame = video_capture.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30)
    )

    # Draw a rectangle around the faces
    for (x, y, w, h) in faces:
        roiGray = gray[y:y+h, x:x+w]
        fileName= dirName + "/" + name + str (count) + ".jpg"
        cv2.imwrite(fileName,roiGray)
        cv2.imshow('image',frame)
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0),2)

        count +=1

    # Display the resulting frame
    #cv2.imshow('Video', frame)


    if cv2.waitKey(1) & 0xFF == ord('a'):
        break
    elif count >= 30 :
        print("Grabado Satifactoriamente")
        break

# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()