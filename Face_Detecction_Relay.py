import cv2
#from picamera.array import PiRGBArray
#from picamera import PiCamera
import numpy as np
import pickle
#import RPi.GPIO as GPIO
from time import sleep

"""
relay_pin = [26]
GPIO.setmode(GPIO.BCM)
GPIO.setup(relay_pin, GPIO.OUT)
GPIO.output(relay_pin, 0)
"""
with open('labels', 'rb') as f:
    dict = pickle.load(f)
    f.close()
cascPath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascPath)
video_capture = cv2.VideoCapture('rtsp://admin:colocolo@192.168.0.100/live')
#faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("trainer.yml")

font = cv2.FONT_HERSHEY_SIMPLEX
while True:

    ret, frame = video_capture.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30)
    )

    for (x, y, w, h) in faces:


        roiGray = gray[y:y+h, x:x+w]

        id_, conf = recognizer.predict(roiGray)
        print (conf)

        for name, value in dict.items():
            if value == id_:
                print(name)
        if conf <= 130:
            print("Reconocido")
            #GPIO.output(relay_pin, 1)
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
            cv2.putText(frame, name + str(conf), (x, y), font, 2, (0, 0 ,255), 2,cv2.LINE_AA)
            break
        else:
            print("No es de la casa")
            #GPIO.output(relay_pin, 0)
    cv2.imshow('image', frame)
    key = cv2.waitKey(1)
    #video_capture.truncate(0)

    if key == 27:
        break

cv2.destroyAllWindows()